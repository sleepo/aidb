'use strict';

require('./../bootstrap');

const Promise = require('bluebird');
const moment = require('moment');
const sync = require('../app/chars/sync');
const knex = require('../config/db');

let fakeStats = [
    [{
        abyss: 'http://localhost:3000/tests/chars/1463340746648_aion_abyss.ratings.abyss.json',
        kills: 'http://localhost:3000/tests/chars/1463339834682_aion_pvp.ratings.pvp.json',
        exp: 'http://localhost:3000/tests/chars/1463340588209_aion_experience.ratings.experience.json'
    }, moment('2016-05-16 23:55:00').toDate()],
    [{
        abyss: 'http://localhost:3000/tests/chars/1463340746649_aion_abyss.ratings.abyss.json',
        kills: 'http://localhost:3000/tests/chars/1463339834683_aion_pvp.ratings.pvp.json',
        exp: 'http://localhost:3000/tests/chars/1463340588210_aion_experience.ratings.experience.json'
    }, moment('2016-05-16 23:57:00').toDate()],
    [{
        abyss: 'http://localhost:3000/tests/chars/1463340746650_aion_abyss.ratings.abyss.json',
        kills: 'http://localhost:3000/tests/chars/1463339834684_aion_pvp.ratings.pvp.json',
        exp: 'http://localhost:3000/tests/chars/1463340588211_aion_experience.ratings.experience.json'
    }, moment('2016-05-16 23:59:00').toDate()],
    [{
        abyss: 'http://localhost:3000/tests/chars/1463346047622_aion_abyss.ratings.abyss.json',
        kills: 'http://localhost:3000/tests/chars/1463346047622_aion_pvp.ratings.pvp.json',
        exp: 'http://localhost:3000/tests/chars/1463346047622_aion_experience.ratings.experience.json'
    }, moment('2016-05-17 00:05:00').toDate()]

];

knex
    .transaction(function(trx){

        return trx.raw('SET foreign_key_checks = 0;')
            .then(function(){

                return Promise.all([
                    'chars',
                    'chars_history',
                    'chars_sync',
                    'guilds',
                    'servers',
                    'servers_online'
                ].map(function(table){
                    return trx.table(table).truncate();
                }));

            })
            .then(function(){
                return trx.raw('SET foreign_key_checks = 0;')
            })
            .then(trx.commit)
            .catch(trx.rollback);

    })
    .then(function(){

        console.log('DATABASE CLEANED UP');

        return Promise.each(fakeStats, function(stat, index){
            console.log('SYNC ' + ( index + 1 ) + ' START');
            return sync(stat[0], stat[1]).then(function(){
                console.log('SYNC ' + ( index + 1 ) + ' END');
            });
        });

    }).catch(function(err){
    throw err;
});


