'use strict';

const express = require('express');
const Promise = require('bluebird');
const md5 = require('md5');
const moment = require('moment');
const knex = require('./../../config/db');
const helpers = require('./../chars/helpers');

module.exports = function(){

    let app = express.Router();

    app.get('/syncs/last', function(req, res, next){
        knex('chars_sync').where({ lock: false }).orderBy('id', 'DESC').first().then(function(record){
            return res.json(record);
        }).catch(next);
    });

    app.get('/servers/stats/online', function(req, res, next){

        var startDate = moment().subtract(7, 'days'),
            endDate = moment();

        if (req.query.dates){
            var dates = req.query.dates.split(' - '),
                startDateTemp = moment(dates[0], 'YYYY-MM-DD'),
                endDateTemp = moment(dates[1], 'YYYY-MM-DD');

            if (
                startDateTemp.isValid() &&
                endDateTemp.isValid() &&
                endDate.isAfter( startDate )
            ){
                startDate = startDateTemp;
                endDate = endDateTemp;
            }
        }

        knex('servers_online')
            .join('servers', 'servers.id', '=', 'servers_online.server_id')
            .select('servers_online.*', 'servers.name')
            .orderBy('created_at', 'ASC')
            .where('servers_online.created_at', '<=', endDate.format('YYYY-MM-DD'))
            .where('servers_online.created_at', '>=', startDate.format('YYYY-MM-DD'))
            .then(function(result){
                return res.json({
                    filters: {
                        dates: startDate.format('YYYY-MM-DD') + ' - ' + endDate.format('YYYY-MM-DD')
                    },
                    result
                });
            });

    });

    app.get('/servers', function(req, res, next){

        knex('servers').whereNull('deleted_at').orderBy('id', 'ASC').select().then(function(result){
            return res.json({
                items: result
            });
        }).catch(next);

    });

    app.get('/races', function(req, res, next){
        return res.json({
            items: helpers.races
        });
    });

    app.get('/classes', function(req, res, next){
        return res.json({
            items: helpers.classes
        });
    });

    app.get('/ranks', function(req, res, next){
        return res.json({
            items: helpers.ranks
        });
    });

    app.get('/chars', function(req, res, next){

        let limit = 100,
            params = {
                page: 1,
                race: null,
                server_id: null,
                'class': null,
                s: null,
                sort: null,
                order: null
            };

        let query = knex('chars')
            .join('servers', 'servers.id', '=', 'chars.server_id')
            .join('guilds', 'guilds.id', '=', 'chars.guild_id');

        if (req.query.page){
            let page = parseInt(req.query.page);
            if (page > 1){
                params.page = page;
            }
        }

        if (req.query.race && helpers.races.indexOf( req.query.race ) !== -1){
            query.where('chars.race', '=', req.query.race);
        }

        if (req.query.server_id){
            query.where('chars.server_id', '=', req.query.server_id);
        }

        if (req.query.class && helpers.classes.indexOf(req.query.class) !== -1){
            query.where('chars.class', '=', req.query.class);
        }

        if (req.query.s){
            query.where('chars.char', 'LIKE', '%' + req.query.s + '%');
        }

        if (req.query.sort && [ 'exp', 'abyss_point', 'glory_point', 'kill' ].indexOf(req.query.sort) !== -1){
            if (req.query.order && ['ASC', 'DESC'].indexOf(req.query.order) !== -1){
                if (req.query.sort === 'exp'){
                    query.orderBy('level', req.query.order);
                    query.orderBy('exp', req.query.order);
                } else {
                    query.orderBy(req.query.sort, req.query.order);
                }
            }
        }

        Promise.props({
            total: query.clone().count().first(),
            items: query
                .limit(limit)
                .offset((params.page - 1) * limit)
                .select('chars.*', 'servers.name as server', 'guilds.name as guild')
        }).then(function(result){
            return res.json({
                total: result.total[ 'count(*)' ],
                items: result.items.map(function(char){
                    char.exp_percent = helpers.expToLevelPercent(char.level, char.exp );
                    return char;
                }),
                params: params
            });
        }).catch(next);


    });

    return app;
};