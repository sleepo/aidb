'use strict';

let _ = require('lodash');

module.exports.races =  ['Элийцы', 'Асмодиане'];

module.exports.normalizeRace = function(race){

    if (module.exports.races.indexOf(race) !== -1){
        return race;
    }

    if ( race.indexOf( 'Э' ) === 0 || race.indexOf('ы') === (race.length - 1) ){
        return module.exports.races[ 0 ];
    }

    if ( race.indexOf( 'А' ) === 0 || race.indexOf( 'е' ) === (race.length - 1) ){
        return module.exports.races[ 1 ];
    }

    throw new Error( "Invalid race value " + race );
};

module.exports.classes = [
    'Целитель',
    'Волшебник',
    'Чародей',
    'Убийца',
    'Гладиатор',
    'Бард',
    'Заклинатель',
    'Страж',
    'Стрелок',
    'Снайпер',
    'Пилот'
];

module.exports.ranks = [
    'Военачальник',
    'Командующий',
    'Великий генерал',
    'Генерал',
    'Офицер 5-го ранга',
    'Офицер 4-го ранга',
    'Офицер 3-го ранга',
    'Офицер 2-го ранга',
    'Офицер 1-го ранга',
    'Воин'
];

module.exports.charId = function(server, race, cl, char){

    let md5 = require('md5');

    if ( typeof server === 'object'){
        char = server.char;
        race = server.race;
        cl = server.class;
        server = server.server;
    }

    return md5( server + race + cl + char );

};

module.exports.guildId = function(server, race, guild){

    let md5 = require('md5');

    if (typeof server === 'object'){
        guild = server.guild;
        race = server.race;
        server = server.server;
    }

    if (!guild){
        guild = '<Без гильдии>';
    }

    return md5(server + race + guild);
};

module.exports.serverId = function(server){

    let md5 = require('md5');

    if (typeof server === 'object'){
        server = server.server;
    }

    return md5(server);

};

module.exports.populateChars = function(lists){

    let charsIndex = {},
        chars = [];

    let _charRef = function(char){
        let id = module.exports.charId(char);

        if (!(id in charsIndex)){
            let length = chars.push({
                id: id,
                server: char.server,
                rank: char.rank,
                char: char.char,
                race: module.exports.normalizeRace(char.race),
                'class': char.class,
                guild: char.guild,

                glory_point: null,
                abyss_point: null,
                kill: null,
                level: null,
                exp: null,

                _total: 0
            });
            charsIndex[ id ] = length - 1;
        }

        return charsIndex[ id ];
    };

    let fromAbyss = function(){
        lists.abyss.chars.forEach(function(char){
           let charRef = _charRef(char);
            chars[ charRef ]._total++;
            chars[ charRef ].glory_point = char.glory_point;
            chars[ charRef ].abyss_point = char.abyss_point;
        });
    };

    let fromKills = function(){
        lists.kills.chars.forEach(function(char){
            let charRef = _charRef(char);
            chars[ charRef ]._total++;
            chars[ charRef ].kill = char.total_pvp_cnt;
        });
    };

    let fromExp = function(){
        lists.exp.chars.forEach(function(char){
            let charRef = _charRef(char);
            chars[ charRef ]._total++;
            chars[ charRef ].exp = char.exp;
            chars[ charRef ].level = char.level;
        });
    };

    let orderedList = _.sortBy( [
        {  timestamp: lists.abyss.timestamp, callback: fromAbyss },
        {  timestamp: lists.kills.timestamp, callback: fromKills },
        {  timestamp: lists.exp.timestamp, callback: fromExp }
    ], [ 'timestamp' ] );

    orderedList.forEach(function(item){
       item.callback();
    });

    return chars.filter((char) => {
        return char._total === 3;
    });

};

module.exports.getLevels = function(){

    return [
        { level: 66, exp: 140120210 },
        { level: 67, exp: 564541760 },
        { level: 68, exp: 1639578224 },
        { level: 69, exp: 3752976718 },
        { level: 70, exp: 7802330396 },
        { level: 71, exp: 15604375580 },
        { level: 72, exp: 31229819120 },
        { level: 73, exp: 54498016200 },
        { level: 74, exp: 94992815400 },
        { level: 75, exp: 162596142000 }
    ];

};

module.exports.expToLevelPercent = function(lvl, exp){

    let level = _.first(module.exports.getLevels().filter(function(item){
        return item.level === lvl + 1;
    }));

    if (!level){
        return 0;
    }

    return ( exp / level.exp * 100 ).toFixed(2);

};