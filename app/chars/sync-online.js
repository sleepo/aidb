'use strict';

const moment = require('moment');
const Promise = require('bluebird');
const util = require('util');

let syncOnline = function(trx, prevSync, lastSync){

    if (!prevSync || !prevSync.id){
        return Promise.resolve(true);
    }

    let keepOnline = moment(lastSync.created_at).diff( prevSync.created_at, 'minutes') < 60;

    return getCharsOnline(trx, prevSync)
        .then(function(chars){
            return Promise.all([
                updateServersOnline(trx, chars, keepOnline),
                updateCharsOnline(trx, chars, prevSync, lastSync, keepOnline)
            ]);
        });

};

module.exports = syncOnline;

function getCharsOnline(trx, prevSync){
    let sql = `
        SELECT chars.id, chars.race, chars.server_id FROM chars
        JOIN chars_history ON ( chars_history.char_id = chars.id )
        WHERE sync_id = ${prevSync.id}
        AND (
            chars.race != chars_history.race OR
            chars.class != chars_history.class OR
            chars.kill != chars_history.kill OR
            chars.guild_id != chars_history.guild_id OR
            chars.abyss_point != chars_history.abyss_point OR
            chars.level != chars_history.level OR
            chars.exp != chars_history.exp
         );
    `;

    return trx.raw( sql ).then(function(chars){
        return chars[0];
    });
}

function updateServersOnline(trx, chars, keepOnline){

    if ( !keepOnline ){
        return true;
    }

    return trx.table('servers').select().then(function(servers){

        let statsByServer = {};

        servers.forEach(function(server){
            statsByServer[server.id] = {
                asmo: 0,
                elyos: 0
            };
        });

        chars.forEach(function(char){
            char.race === 'Элийцы' ? statsByServer[char.server_id].elyos++ : statsByServer[char.server_id].asmo++;
        });

        let recordsToInsert = [],
            now = new Date();

        for (let server_id in statsByServer){
            recordsToInsert.push({
                server_id: server_id,
                asmo: statsByServer[server_id].asmo,
                elyos: statsByServer[server_id].elyos,
                created_at: now
            });
        }

        return trx.table('servers_online').insert( recordsToInsert );

    });

}

function updateCharsOnline(trx, chars, prevSync, lastSync, keepOnline){

    // continue online day session
    return trx.table('chars_history').where('online', '>', 0).where('sync_id', '=', prevSync.id).select().then(function(result){

        let usePrev = moment(prevSync.created_at).format('YYYY-MM-DD') === moment(lastSync.created_at).format('YYYY-MM-DD');

        return Promise.map(result, function(item){
            // chars.online not resettings
            return trx.table('chars_history').where('char_id', '=', item.char_id).where('sync_id', '=', lastSync.id).update({
                online: usePrev ? item.online : 0
            }).then(function(){

            });
        });

    }).then(function(){

        if (!keepOnline){
            return Promise.resolve();
        }

        // update current online users
        return Promise.map(chars, function(char){

            return trx.table('chars_history')
                .where('sync_id', '=', lastSync.id)
                .where('char_id', '=', char.id)
                .first()
                .then(function(result){

                    let newOnline = result.online + 1;

                    return Promise.all([
                        trx.table('chars').where('id', '=', char.id).update({ online: newOnline, now_online: 1 }).then(function(){

                        }),
                        trx.table('chars_history').where('char_id', '=', char.id).where('sync_id', '=', lastSync.id).update({ online: newOnline }).then(function(){

                        })
                    ]);

                });

        });
    });

}