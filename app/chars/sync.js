'use strict';

const got = require('got'),
      Promise = require('bluebird'),
      _ = require('lodash'),
      md5 = require('md5'),
      moment = require('moment');

const knex = require('./../../config/db');
const helpers = require('./helpers');
const syncOnline = require('./sync-online');

let needUpdateData = function(url, lastEtag){

    let headers = {};

    if (lastEtag){
        headers['If-None-Match'] = lastEtag;
    }

    return got.head( url, {
        headers,
        json: true
    } ).then(function(resp){
        return { etag: resp.headers.etag };
    }).catch(function(err){
        if (err.statusCode === 304){
            return false;
        }
        throw err;
    });

};

let downloadData = function(url){
    return got(url, {
        json: true
    }).then(function(resp){
        return {
            timestamp: resp.body[0].timestamp,
            chars: resp.body[1].chars
        };
    });
};

let needUpdate = function(urls){

    let lastSyncGlobal;

    return Promise.resolve(new Date())
        .then(function(){
            return knex('chars_sync').orderBy('id', 'DESC').first();
        }).then(function(lastSync){
            lastSyncGlobal = lastSync = lastSync || {};

            if (lastSync.lock){
                let err = new Error("Sync in progress");
                err.name = 'SyncInProgress';
                throw err;
            }

            return Promise.map(Object.keys(urls), function(type){
                return needUpdateData(urls[type], lastSync[ type + '_etag' ])
                    .then(function(update){
                        return update ? { etag: update.etag, type: type } : false;
                    });
            });
        }).then(function(needUpdates){
            needUpdates = needUpdates.filter((item) => item );

            if (needUpdates.length !== Object.keys(urls).length){
                let err = new Error("Already fresh data.");
                err.name = 'SyncNoNeedUpdate';
                throw err;
            }

            return {
                needUpdates: needUpdates,
                prevSync: lastSyncGlobal
            };
        });

};

let doSync = function(urls, sync, trx){

    return Promise.props({
        abyss: downloadData(urls.abyss),
        kills: downloadData(urls.kills),
        exp: downloadData(urls.exp)
    }).then(function(data){
        return trx.table('chars_sync').where({id: sync.current.id}).update({
            abyss_timestamp: data.abyss.timestamp,
            kills_timestamp: data.kills.timestamp,
            exp_timestamp: data.exp.timestamp
        }).then(() => data);
    }).then(function(data){
        return _.chunk( helpers.populateChars(data), 1000 );
    }).then(function(chunks){
        return Promise.each(chunks, function(chunk){
            return insertServers(trx, chunk).then(function(){
                return insertGuilds(trx, chunk);
            }).then(function(){
                return insertChars(trx, chunk);
            }).then(function(){
                return insertAttributes(trx, chunk, sync.current.id);
            });
        });
    }).then(function(){
         return syncOnline(trx, sync.prev, sync.current);
    });

};

let insertServers = function(trx, chunk){

    let records = _.uniq(chunk.map(function(item){
        return item.server;
    }));

    records = records.map(function(item){
        return [ md5(item), item ];
    });

    let sql = 'INSERT INTO servers (id, name, created_at) VALUES ';
    sql += records.map(() => '(?,?, NOW())').join(',');
    sql += ' ON DUPLICATE KEY UPDATE updated_at=NOW(), deleted_at=NULL;';

    return trx.raw(sql, _.flatten(records));

};

let insertGuilds = function(trx, chunk){

    let unique = {};

    chunk.forEach(function(item){

        let key = item.server + item.race + item.guild;

        if (!unique[key]){
            unique[key] = {
                id: helpers.guildId(item),
                server_id: helpers.serverId(item),
                name: item.guild,
                race: item.race
            };
        }

    });

    let records = [];
    for (let i in unique){
        records.push( [ unique[i].id, unique[i].server_id, unique[i].race, unique[i].name ] );
    }

    let sql = 'INSERT INTO guilds (id, server_id, race, name, created_at, updated_at) VALUES ';
    sql += records.map(() => '(?, ?, ?, ?, NOW(), NOW())').join(',');
    sql += ' ON DUPLICATE KEY UPDATE updated_at=NOW(), deleted_at=NULL;';

    return trx.raw(sql, _.flatten(records));
};

let insertChars = function(trx, chunk){

    let chars = chunk.map(function(char){
        let charId = helpers.charId(char),
            serverId = helpers.serverId(char),
            guildId = helpers.guildId(char);

        return [
            charId,
            serverId,
            guildId,
            char.race,
            char.class,
            char.char,
            char.glory_point,
            char.abyss_point,
            char.kill,
            char.level,
            char.exp,
            char.rank
        ];
    });

    let sql = 'INSERT INTO chars (`id`, `server_id`, `guild_id`, `race`, `class`, `char`, `glory_point`, `abyss_point`, `kill`, `level`, `exp`, `rank`, `created_at`, `updated_at`) VALUES ';
    sql += chars.map(() => '(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW())').join(',');
    sql += ' ON DUPLICATE KEY UPDATE `server_id` = VALUES(`server_id`), `guild_id` = VALUES(`guild_id`), `race` = VALUES(`race`), `class` = VALUES(`class`), `char` = VALUES(`char`), `glory_point` = VALUES(`glory_point`), `abyss_point` = VALUES(`abyss_point`), `kill` = VALUES(`kill`), `level` = VALUES(`level`), `exp` = VALUES(`exp`), `rank` = VALUES(`rank`), deleted_at=NULL, updated_at=NOW();';

    return trx.raw(sql, _.flatten(chars));
};

let insertAttributes = function(trx, chunk, syncId){

    let attributes = [];

    for (let i = 0; i < chunk.length; i++) {
        let char = chunk[i],
            charId = helpers.charId(char);

        attributes.push({
            char_id: charId,
            sync_id: syncId,
            guild_id: helpers.guildId(char),
            race: char.race,
            'class': char.class,
            level: char.level,
            rank: char.rank,
            abyss_point: char.abyss_point,
            glory_point: char.glory_point,
            kill: char.kill,
            exp: char.exp
        });

    }

    return trx.table('chars_history').insert( attributes );

};

module.exports = function(urls, createdAt){

    createdAt = createdAt || new Date();

    return needUpdate(urls)
        .then(function(data){

            let sync = {
                abyss_url: urls.abyss,
                kills_url: urls.kills,
                exp_url: urls.exp,
                created_at: createdAt,
                lock: true
            };
            data.needUpdates.forEach(function(item){
                sync[ item.type + '_etag' ] = item.etag;
            });

            return knex('chars_sync').insert(sync).returning('id').then((ids) =>  {
                sync.id = ids[0];
                return {
                    prev: data.prevSync,
                    current: sync
                };
            });

        })
        .then(function(sync){
            let transactionError = null;
            return knex
                .transaction(function(trx){
                    return Promise.resolve()
                        .then(function(){
                            let now = new Date();
                            return Promise.all([
                                trx.table('servers')
                                   .whereNull('deleted_at')
                                   .update({ deleted_at: now }),
                                trx.table('guilds')
                                    .whereNull('deleted_at')
                                    .update({ deleted_at: now }),
                                trx.table('chars')
                                    .whereNull('deleted_at')
                                    .update({ deleted_at: now }),
                                trx.table('chars')
                                    .update({ online: 0, now_online: 0 })
                            ]);
                        })
                        .then(function(){
                            return doSync(urls, sync, trx);
                        })
                        .then(function(){
                            let needDeletePrevSync =  sync.prev && sync.prev.id &&
                                moment(sync.prev.created_at).format('YYYY-MM-DD') === moment(sync.current.created_at).format('YYYY-MM-DD');

                            if (needDeletePrevSync){
                                return trx.table('chars_sync').where({id: sync.prev.id}).del();
                            } else {
                                return Promise.resolve()
                            }
                        })
                        .then(function(){
                            return trx.table('chars_sync').where({id: sync.current.id}).update({
                                lock: false,
                                updated_at: new Date()
                            }).then(() => sync);
                        })
                        .then(trx.commit)
                        .catch(function(err){
                            transactionError = err;
                            return trx.rollback();
                        });
                })
                .catch(function(err){
                    if (!err){
                        err = transactionError;
                    }
                    return knex('chars_sync').where({ id: sync.current.id }).del().then(function(){
                        throw err;
                    });
                });
        })
        .then(() => true)
        .catch(function(err){
            if (err && err.name === 'SyncInProgress'){
                return false;
            }

            if (err && err.name === 'SyncNoNeedUpdate'){
                return false;
            }
            throw err;
        });

};