'use strict';

module.exports = ['$scope', 'ApiService', '$timeout', 'moment', '$filter', '$stateParams', '$state', function($scope, ApiService, $timeout, moment, $filter, $stateParams, $state){

    $scope.loading = true;

    $scope.filters = {
        dates: $stateParams.dates
    };

    $scope.options = {
        chart: {
            type: 'multiBarChart',
            height: 600,
            showControls: false,
            grouped: true,
            tooltip: {
                contentGenerator: function(row){

                    let total = row.data.elyos + row.data.asmo,
                        time = $filter('amDateFormat')(row.data.created_at, 'HH:mm:ss MM-DD-YYYY');

                    return `<div class="tp">
                                <p class="tp-header">${row.data.name}</p>
                                <p>Всего: ${total}</p>
                                <p>Элийцы: ${row.data.elyos}</p>
                                <p>Асмодиане: ${row.data.asmo}</p>
                                <p class="tp-footer">${time}</p>
                            </div>`;
                }
            },
            "xAxis": {
                "axisLabel": null,
                "tickFormat": function(v){
                    return moment(v).format("ddd в HH:mm");
                }
            },
            "yAxis": {
                "axisLabel": null,
                "tickFormat": function(v){
                    return $filter('cm_number')(v);
                }
            },
            x: function(d){
                return moment(d.created_at).toDate();
            },
            y: function(d){
                return d.asmo + d.elyos;
            }
        }
    };
    $scope.data = [];

    $scope.$watch('filters', function(value){
        $state.go('.', value);
    }, true);

    function fetchData(){
        return ApiService.getServersStats( $scope.filters ).then(function(result){

            let servers = {};

            $scope.filters.dates = result.filters.dates;

            result.result.forEach(function(item){

                if (!servers[item.name]){
                    servers[item.name] = [];
                }

                servers[item.name].push( item )
            });

            let r = [];

            for (let i in servers){
                r.push( {
                    key: i,
                    values: servers[i]
                } );
            }

            return r;

        });
    }

    fetchData().then(function(result){
        $timeout(function(){
            $scope.loading = false;
            $scope.data = result;
        }, 0);
    });


}];