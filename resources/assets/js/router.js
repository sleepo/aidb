'use strict';

module.exports = function($stateProvider, $urlRouterProvider, $locationProvider){

    $locationProvider.html5Mode(true);

    $urlRouterProvider.otherwise('/chars');

    $stateProvider
        .state('chars', {
            url: '/chars?s&page&server_id&race&class&sort&order',
            templateUrl: '/views/chars/index.html',
            controller: 'CharsListCtrl',
            reloadOnSearch: false
        })
        .state('serversOnline', {
            url: '/servers/online?dates',
            templateUrl: '/views/servers/index.html',
            controller: 'ServersListCtrl'
        });
};