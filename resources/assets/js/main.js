'use strict';

let moment = require('moment'),
    jquery = require('jquery'),
    angular = require('angular');

// Load time lib
require('moment/locale/ru');
window.moment = require('moment-timezone');
require('angular-moment');

// Load chart lib
require('nvd3');
require('angular-nvd3');

require('ng-daterangepicker');

let app = angular.module('Aion', [
    require('angular-ui-router'),
    require('angular-ui-bootstrap'),
    'jkuri.daterangepicker',
    'angularMoment',
    'nvd3'
]);
app.constant('moment', require('moment-timezone'));
app.constant('Promise', require('bluebird'));
app.constant('_', require('lodash'));

app.filter('cm_number', require('./cm-number-filter'));
app.factory('ApiService', require('./api-service'));

app.controller('CharsListCtrl', require('./chars/chars-list-ctrl'));
app.controller('ServersListCtrl', require('./servers/servers-list-ctrl'));

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', require('./router')]);

app.run(require('./run'));