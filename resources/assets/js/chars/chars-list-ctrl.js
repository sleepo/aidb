'use strict';

module.exports = [
    '$scope', '$http', 'Promise', '_', '$timeout', '$state', '$stateParams', '$interval', 'ApiService',
    function($scope, $http, Promise, _, $timeout, $state, $stateParams, $interval, ApiService){

    $scope.servers = [];
    $scope.classes = [];
    $scope.races = [];

    $scope.form = {};
    $scope.defaults = {
        server_id: null,
        race: null,
        'class': null,
        s: null,
        sort: 'exp',
        order: 'DESC',
        page: 1
    };

    $scope.needUpdate = true;
    $scope.chars = [];
    $scope.sync = null;

    $scope.onFormChange = function(){
        $scope.form.page = $scope.defaults.page;
        $state.go('.', $scope.form);
    };

    $scope.onPageChange = function(){
        $state.go('.', $scope.form);
    };

    $scope.changeSortBy = function(field){

        if ($scope.form.sort !== field){
            $scope.form.sort = field;
            $scope.form.order = $scope.defaults.order;
        } else {
            $scope.form.order = $scope.form.order === 'DESC' ? 'ASC' : 'DESC';
        }

        $scope.onFormChange();
    };

    $scope.updateChars = function(form, sync){
        form = form || $scope.form;
        $scope.charsLoading = true;

        ApiService.getChars(form).then(function(resp){
            $timeout(function(){
                $scope.chars = resp.items;

                $scope.totalItems = resp.total;

                if (sync){
                    $scope.sync = sync;
                }

                $scope.charsLoading = false;
            }, 0);
        });
    };

    $scope.loading = $scope.charsLoading = true;

    function initialize(){
        return Promise.props({
            servers: ApiService.getServers(),
            classes: ApiService.getClasses(),
            races: ApiService.getRaces(),
            sync: ApiService.getSyncLast()
        }).then(function(props){

            $timeout(function(){
                $scope.servers = props.servers;
                $scope.classes = props.classes;
                $scope.races = props.races;
                $scope.sync = props.sync;

                _.defaults( $scope.form, $stateParams, $scope.defaults );

                $scope.loading = false;

                $scope._updateInterval = $interval(function(){
                    ApiService.getSyncLast().then(function(result){
                        if (result.id !== $scope.sync.id){
                            $scope.updateChars($scope.form, result);
                       }
                    });

                }, 30 * 1000);

                $scope.$on('$destroy', function(){
                   $interval.cancel($scope._updateInterval);
                });

                $scope.$watch('form', function(value){
                   $scope.updateChars(value);
                }, true);

            }, 0);

        });
    }

    initialize();

}];