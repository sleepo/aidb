'use strict';

module.exports = ['$http', 'Promise', function($http, Promise){

    let apiBaseUrl = '';

    function getServers(){
        return new Promise(function(res, rej){
            $http.get(apiBaseUrl + '/api/servers').success(function(resp){
                return res(resp.items);
            }).catch(rej);
        });
    }

    function getClasses(){
        return new Promise(function(res, rej){
            $http.get(apiBaseUrl + '/api/classes').success(function(resp){
                return res(resp.items);
            }).catch(rej);
        });
    }

    function getRaces(){
        return new Promise(function(res, rej){
            $http.get(apiBaseUrl + '/api/races').success(function(resp){
                return res(resp.items);
            }).catch(rej);
        });
    }

    function getChars(params){
        return new Promise(function(res, rej){
            $http.get(apiBaseUrl + '/api/chars', {
                params
            }).success(function(resp){
                return res(resp);
            }).catch(rej);
        });
    }

    function getSyncLast(){
        return new Promise(function(res, rej){
            $http.get(apiBaseUrl + '/api/syncs/last').success(function(result){
                return res(result);
            }).catch(rej);
        });
    }

    function getServersStats(params){
        return new Promise(function(res, rej){
            $http.get(apiBaseUrl + '/api/servers/stats/online', {
                params
            }).success(function(result){
                return res(result);
            }).catch(rej);
        });
    }

    return { getServers, getClasses, getRaces, getChars, getSyncLast, getServersStats };


}];