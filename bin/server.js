'use strict';

const express = require('express');
const app = express();

// Configure
require('./../bootstrap');
require('./../config/views')(app);

app.use(express.static( process.cwd() + '/public' ));
app.use('/api', require('./../app/api')());
app.use('/*', function(req, res){
    res.locals.elixir = require('./../config/elixir');
    res.locals.counter = process.env.NODE_ENV === 'production';
    return res.render('index.html', {});
});

app.listen( process.env.SERVER_PORT );