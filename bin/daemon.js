'use strict';

require('./../bootstrap');

const cron = require('cron');
const sync = require('../app/chars/sync');

let lock = false;

let syncJob = function(){
    return new cron.CronJob({
        cronTime: '0 */3 * * * *',
        onTick: function() {
            if ( !lock ){
                console.log('sync start');
                lock = true;
                sync({
                    abyss: 'https://cdn.inn.ru/webdav/ratings/files/aion_abyss.ratings.abyss.json',
                    kills: 'https://cdn.inn.ru/webdav/ratings/files/aion_pvp.ratings.pvp.json',
                    exp: 'https://cdn.inn.ru/webdav/ratings/files/aion_experience.ratings.experience.json'
                }).then(function(isFinished){
                    lock = false;
                    console.log('sync finished' + ( !isFinished ? ', not required' : ''));
                }).catch(function(err){
                    lock = false;
                    throw err;
                });
            } else {
                console.error('Sync is locked');
            }
        },
        start: false,
        runOnInit: true,
        timeZone: process.env.TIME_TIMEZONE
    });
};

syncJob().start();

