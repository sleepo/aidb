var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.less('main.less');
    mix.browserify('main.js');
    if (elixir.config.production){
        mix.version(['css/main.css', 'js/main.js']);
    }
    mix.copy('resources/assets/views', 'public/views');
    mix.copy('node_modules/font-awesome/fonts', 'public/vendor/font-awesome/fonts');
});