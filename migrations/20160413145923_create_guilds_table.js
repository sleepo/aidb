
exports.up = function(knex, Promise) {
  return knex.schema.createTable('guilds', function(table){
      table.string('id', 32);
      table.string('server_id', 32).references('id').inTable('servers');
      table.string('name');
      table.enum('race', ['Асмодиане', 'Элийцы']);
      table.dateTime('created_at');
      table.dateTime('updated_at');
      table.dateTime('deleted_at');
      table.primary('id');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('guilds');
};
