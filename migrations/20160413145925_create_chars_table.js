'use strict';

const helpers = require('../app/chars/helpers');

exports.up = function(knex, Promise) {
    return knex.schema.createTable('chars', function(table){
        table.string('id', 32);
        table.string('server_id', 32).references('id').inTable('servers');
        table.string('guild_id', 32).references('id').inTable('guilds');
        table.enum('race', [ 'Элийцы', 'Асмодиане' ]);
        table.string('char');
        table.enum('class', helpers.classes);
        table.enum('rank', helpers.ranks);
        table.integer('abyss_point').unsigned();
        table.integer('glory_point').unsigned();
        table.integer('kill').unsigned();
        table.integer('level').unsigned();
        table.bigInteger('exp').unsigned();

        table.dateTime('created_at');
        table.dateTime('updated_at');
        table.dateTime('deleted_at');

        table.primary('id');
    });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('chars');
};
