
exports.up = function(knex, Promise) {
  return knex.schema.createTable('servers', function(table){
        table.string('id', 32);
        table.string('name');
        table.dateTime('created_at');
        table.dateTime('updated_at');
        table.dateTime('deleted_at');
        table.primary('id');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('servers');
};
