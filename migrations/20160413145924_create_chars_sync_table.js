
exports.up = function(knex, Promise) {
    return knex.schema.createTable('chars_sync', function(table){
        table.increments('id');
        table.string('abyss_url');
        table.string('abyss_timestamp');
        table.string('abyss_etag');
        table.string('kills_url');
        table.string('kills_timestamp');
        table.string('kills_etag');
        table.string('exp_url');
        table.string('exp_timestamp');
        table.string('exp_etag');
        table.bool('lock');
        table.dateTime('created_at');
        table.dateTime('updated_at');
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('chars_sync')
};
