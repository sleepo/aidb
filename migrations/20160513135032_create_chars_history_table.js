'use strict';

const helpers = require('../app/chars/helpers');

exports.up = function(knex, Promise) {
    return knex.schema.createTable('chars_history', function(table){
        table.bigIncrements('id');

        table.string('guild_id', 32).references('id').inTable('guilds');
        table.string('char_id', 32).references('id').inTable('chars');
        table.integer('sync_id').unsigned().references('id').inTable('chars_sync').onDelete('CASCADE');

        table.enum('race', helpers.races);
        table.enum('class', helpers.classes);
        table.enum('rank', helpers.ranks);
        table.integer('abyss_point').unsigned();
        table.integer('glory_point').unsigned();
        table.integer('kill').unsigned();
        table.integer('level').unsigned();
        table.bigInteger('exp').unsigned();
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('chars_history');
};
