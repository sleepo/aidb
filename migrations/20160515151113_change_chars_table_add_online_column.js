'use strict';

exports.up = function(knex, Promise) {
    return knex.schema.table('chars', function(table){
        table.integer('online').unsigned().default(0);
        table.boolean('now_online');
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.table('chars', function(table){
        table.dropColumn('online');
        table.dropColumn('now_online');
    });
};
