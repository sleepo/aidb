'use strict';

exports.up = function(knex, Promise) {
    return knex.schema.createTable('servers_online', function(table){
        table.bigIncrements('id');
        table.string('server_id', 32).references('id').inTable('servers');
        table.integer('asmo');
        table.integer('elyos');
        table.dateTime('created_at');
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('servers_online');
};
