'use strict';

exports.up = function(knex, Promise) {
    return knex.schema.table('chars_history', function(table){
        table.integer('online').unsigned().default(0);
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.table('chars_history', function(table){
        table.dropColumn('online');
    });
};
