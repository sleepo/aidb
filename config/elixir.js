'use strict';

let config = {};

try {
    config = require('./../public/build/rev-manifest.json');
} catch (err){
    if (err.code === 'MODULE_NOT_FOUND'){
        console.warn('rev-manifest.json not found');
    } else {
        throw err;
    }
}

module.exports = function(path){
    return path in config ? '/build/' + config[path] : '/' + path;
};