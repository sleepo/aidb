'use strict';

const nunjucks = require('nunjucks');

module.exports = function(app){

    let options = {
        watch: false,
        autoescape: true
    };

    if (app){
        options.express = app;
    }

    nunjucks.configure( process.cwd() + '/resources/views', options);
};

